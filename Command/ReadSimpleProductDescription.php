<?php

namespace Mbs\SimpleDescription\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReadSimpleProductDescription extends Command
{
    /**
     * @var \Mbs\SimpleDescription\Model\ProductLoader
     */
    private $productLoader;

    public function __construct(
        \Mbs\SimpleDescription\Model\ProductLoader $productLoader,
        string $name = null
    ) {
        parent::__construct($name);
        $this->productLoader = $productLoader;
    }

    protected function configure()
    {
        $this->setName('mbs:product:finddescription');
        $this->setDescription('Find Simple Product Description');

        $this->addArgument('sku', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $product = $this->productLoader->getProductFromSku($input->getArgument('sku'));

        if ($product) {
            $output->writeln(sprintf('product sku: %s has description %s', $product->getSku(), $product->getDescription()));
        }

        $output->writeln('Found product description');
    }
}
