<?php

namespace Mbs\SimpleDescription\Plugin;

use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Registry;

class ProductDataReader
{
    /**
     * @var Registry
     */
    private $registry;
    /**
     * @var \Mbs\SimpleDescription\Logger
     */
    private $logger;

    public function __construct(
        Registry $registry,
        \Mbs\SimpleDescription\Logger $logger
    ) {
        $this->registry = $registry;
        $this->logger = $logger;
    }

    public function aroundGetData(
        \Magento\Catalog\Model\Product $subject,
        callable $proceed,
        $attribute = '',
        $index = null
    ) {
        if ($attribute == 'description') {
            if ($this->isProductSimpleFromConfigurable($subject)) {
                $this->logger->addLog('Product with simple product from configurable: ' . $subject->getSku());
                $mainProduct = $this->getMainConfigurableProduct();
                if ($mainProduct) {
                    $this->logger->addLog('Product has parent: ' . $mainProduct->getSku());
                    $result = $mainProduct->getDescription();
                } else {
                    $this->logger->addLog('Product is have no parent in registry');
                }
            } else {
                $result = $proceed($attribute, $index);
            }
        } else {
            $result = $proceed($attribute, $index);
        }

        return $result;
    }

    /**
     * @param \Magento\Catalog\Model\Product $subject
     * @return bool
     */
    private function isProductSimpleFromConfigurable(\Magento\Catalog\Model\Product $subject): bool
    {
        return  $subject->getVisibility()==\Magento\Catalog\Model\Product\Visibility::VISIBILITY_NOT_VISIBLE
            and $subject->getTypeId() === \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE;
    }

    private function getMainConfigurableProduct()
    {
        $mainProduct = $this->registry->registry('current_product');

        if ($mainProduct and $mainProduct->getTypeId() === Configurable::TYPE_CODE) {
            return $mainProduct;
        }

        return false;
    }
}
