# README #

Revert to parent product description if a simple product has no description

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/SimpleDescription when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

run php bin/magento mbs:product:finddescription <sku> to verify if the module runs successfully

find a product configurable with simple product descriptions empty and verify the description rendered is either the simple if the latter is not empty or is the parent
